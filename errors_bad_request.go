package goerr

import "github.com/pkg/errors"

type BadRequest struct {
	BaseError
}

func NewBadRequest(e error) BadRequest {
	return BadRequest{NewBaseError(e)}
}

func (e BadRequest) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e BadRequest) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewBadRequest(newErr)
}
