package goerr

import "github.com/pkg/errors"

type Unprocessable struct {
	BaseError
}

func NewUnprocessable(e error) Unprocessable {
	return Unprocessable{NewBaseError(e)}
}

func (e Unprocessable) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e Unprocessable) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewUnprocessable(newErr)
}

func (e Unprocessable) Cause() error {
	return e.err
}
